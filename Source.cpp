#include <iostream>
#include <string>
#include <time.h>
#include "UnorderedArray.h"

using namespace std;

int main()
{
	//srand(time(NULL));
	int num = 0;
	int choice = 0;
	int findNum = 0;

	cout << "Enter amount : \n";
	cin >> num;

	UnorderedArray<int> input(num);
	for (int i = 0; i < num; i++)
	{
		num = rand() % 100;
		input.push(num);
		cout << num << endl;
	}
	cout << endl;

	cout << R"( What would you like to do?
				[1] Remove an element
				[2] Search element )";
	cin >> choice;
	cout << endl;

	if (choice == 1)
	{
		input.remove(num);
		input.pop();
	}
	else if (choice == 2)
	{
		cout << "Enter number: " << endl;
		cin >> findNum;

		for (int i = 0; i < num; i++)
		{
			if (findNum == input[i])
			{
				cout << "Found it! \n";
			}
			else if (findNum != input[num - 1])
			{
				cout << "Oh no! It's not here \n";
			}
		}
	}
	else if (choice != 1 && choice != 2)
	{
		while (choice != 1 && choice != 2)
		{
			cout << "Please try again";
				cin >> choice;
		}
	}

	system("pause");
	system("cls");
}