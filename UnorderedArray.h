#pragma once
#include <assert.h>

template< class T>

class UnorderedArray
{
public:
	UnorderedArray(int size, int growBy = 1) : mInput(NULL), mMaxNum(0), mGrowSize(0), mNumElements(0) 
	{
		if (size)
		{
			mMaxNum = size;

			mInput = new T[mMaxNum];
			memset(mInput, 0, sizeof(T) * mMaxNum);

			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}

	virtual ~UnorderedArray() 
	{
		if (mInput != NULL)
		{
			delete[] mInput;
			mInput = NULL;
		}
	}

	virtual void push(T value)
	{
		assert(mInput != NULL);
		mInput[mNumElements] = value;
		mNumElements++;
	}

	virtual void pop()
	{
		if (mNumElements > 0)
		{
			mNumElements--;
		}
	}

	virtual void remove(int index)
	{
		assert(mInput != NULL);
		{
			return;
		}

		for (int i = index; i < mMaxNum - 1; i++) 
		{
			mInput[i] = mInput[i + 1];
		}

		mMaxNum--;
		if (mNumElements >= mMaxNum)
			mNumElements = mMaxNum;
	}

private:
	T * mInput;
	int mMaxNum; 
	int mGrowSize;
	int mNumElements;

	bool expand()
	{
		if (mGrowSize <= 0)
		{
			return false;
		}

		T *temp = new T[mMaxNum + mGrowSize];
		assert(temp != NULL);

		memcpy(temp, mInput, sizeof(T) * mMaxNum);
		delete[] mInput;

		mInput = temp;
		mMaxNum + mGrowSize;

		return true;
	}
};